﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Manage collisions with targets and handle clean-up
public class Bullet : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		Invoke("DestroySelf", 5f);
	}

	private void OnCollisionEnter(Collision collisionInfo)
	{
		if(collisionInfo.gameObject.tag == "Target")
		{
			Destroy(collisionInfo.gameObject);
		}
	}

	private void DestroySelf()
	{
		Destroy(this.gameObject);
	}
}
