﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Capture player input, instantiate bullets from a serialized prefab, apply force
public class FiringSystem : MonoBehaviour 
{
	[SerializeField]
	private GameObject bulletPrefab;
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetMouseButtonDown(0) && GameManager.isGameActive)
		{
			Fire();
		}
	}

	private void Fire()
	{
		var newBullet = Instantiate(bulletPrefab, transform.position + transform.forward, transform.rotation);
		newBullet.GetComponent<Rigidbody>().AddForce(transform.forward * 25, ForceMode.Impulse);
	}
}
