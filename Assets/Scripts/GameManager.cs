﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Keep track of remaining targets, trigger win and loss conditions, reset scene, update score canvas, display win/loss canvases
public class GameManager : MonoBehaviour 
{
	public static int RemainingTargets;
	public static bool isGameActive;
	public static bool isGameComplete;

	[SerializeField]
	private Text enemiesRemainingText;

	// Use this for initialization
	void Start () 
	{
		isGameActive = false;
		isGameComplete = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!isGameActive)
		{
			if(Input.GetMouseButtonDown(0))
			{
				if(isGameComplete)
				{
					RestartGame();
				}
				else
				{
					StartGame();
				}
			}
		}

		enemiesRemainingText.text = RemainingTargets.ToString();
	}

	private void StartGame()
	{
		Debug.Log("Starting game.");
		Target[] allTargets = FindObjectsOfType<Target>();
		RemainingTargets = allTargets.Length;
		enemiesRemainingText.text = RemainingTargets.ToString();
		isGameActive = true;
	}

	public static void EndGame()
	{
		Debug.Log("Ending game.");
		isGameActive = false;
		isGameComplete = true;
	}

	private void RestartGame()
	{
		Debug.Log("Restarting game.");
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public static void RemoveTarget()
	{
		RemainingTargets--;

		if(RemainingTargets <= 0)
		{
			EndGame();
		}
	}
}
