﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Move towards the player, update GameManager with loss trigger or destroyed state
public class Target : MonoBehaviour 
{
	private float moveSpeed = 0.10f;
	private Vector3 playerPosition;

	// Use this for initialization
	void Start () 
	{
		var player = FindObjectOfType<FiringSystem>();
		playerPosition = player.gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!GameManager.isGameActive)
			return;
		
		transform.LookAt(playerPosition);
		transform.Translate(transform.forward * moveSpeed);
		if(Vector3.Magnitude(transform.position - playerPosition) < 1f)
		{
			GameManager.EndGame();
			Destroy(this.gameObject);
		}
	}

	private void OnDestroy()
	{
		GameManager.RemoveTarget();
	}
}
